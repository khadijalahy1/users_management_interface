package org.userManagement.servlet;


import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
//it's such manipulate users class



//methods de recherche par telephone,par email,par city..
//update
//delete

public class UserDao {
	
	private SessionFactory sessionFactory = null;
	Session session = null;
	Transaction tx = null;

	public UserDao() {

	}
	 
	//get all the users of the db
	public static  List<User> getUsers() {

		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();

		Session session = sessionFactory.openSession();

		Query query = session.createQuery("from User");// here persistent class name is Emp
		// query.setFirstResult(1);
		// query.setMaxResults(2);
		List<User> list = query.list();
		return list;

	}
	
	//To add a new User
	public static void add(String userName, String password, String email, String phone, String city,String cin) {
		Transaction tx = null;
		// TODO Auto-generated method stub
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();

		Session session = sessionFactory.openSession();
		tx = session.beginTransaction();
		User user = new User();
		user.setUserName(userName);
		user.setCity(city);
		user.setEmail(email);
		user.setPassword(password);
		user.setPhone(phone);
		user.setCin(cin);
		
		session.save(user);

		tx.commit();

	}
	
	//update a user
	public static void update(int id,String userName,String password, String email, String phone, String city,String cin) {
		Transaction tx = null;
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();

		Session session = sessionFactory.openSession();
		
		tx = session.beginTransaction();
		Query q = session.createQuery("update User set userName=:s , password=:p ,email=:e,phone=:p,city=:v,cin=:c  where id=:i");
		
		
		q.setParameter("s",userName);
		q.setParameter("p",password);
		q.setParameter("e",email);
		q.setParameter("p",phone);
		q.setParameter("v",city);
		q.setParameter("c",cin);
		q.setParameter("i",id);

		int status = q.executeUpdate();
		System.out.println(status);
		tx.commit();
		
		
	}
	//delete a user
	public static void delete(int id) {
		Transaction tx = null;
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();

		Session session = sessionFactory.openSession();
		tx = session.beginTransaction();
		Query q = session.createQuery("delete from User where id=:i");
		q.setParameter("i",id);
		int status = q.executeUpdate();
		System.out.println(status);
		tx.commit();

		
		
		
	}
	
	//search for user
	public  static  List<User> search(String searchText,String searchField) {
		
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		Query q=session.createQuery("from User  where "+searchField+" like '%"+searchText+"%'");
		
		List<User> list = q.list();
		return list;
		
	}
	

	public static User getUser(int id) {
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();

		Session session = sessionFactory.openSession();
		User user = (User) session.load(User.class, id);
		return user;
	}
}