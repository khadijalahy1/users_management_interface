package org.userManagement.servlet;

//for version 10 we use jakarta instead of javax
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class UserControlerServlet
 */
//annotations 
@WebServlet("/UserControlerServlet")
public class UserControlerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	String action ="";
	int oldUserId=0;
	

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UserControlerServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	// recuperer la requete
	protected void doGet(HttpServletRequest request, HttpServletResponse response/**/)
			throws ServletException, IOException {

		System.out.print("I'm in doGet!");
		List<User> users = UserDao.getUsers();

		action = request.getParameter("action");

		if (action == null) {
			request.setAttribute("users", users);
			request.getRequestDispatcher("HomePage.jsp").forward(request, response);
		}
		if (action.equals("listUsers")) {
			request.setAttribute("users", users);
			request.getRequestDispatcher("HomePage.jsp").forward(request, response);

		}
		if (action.equals("delete")) {
			int userId = Integer.parseInt(request.getParameter("id"));
			UserDao.delete(userId);
			users = UserDao.getUsers();
			request.setAttribute("users", users);
			request.getRequestDispatcher("HomePage.jsp").forward(request, response);

		}
		if (action.equals("goAddPage")) {
			System.out.print("I'm inside do action goAddPage");
			request.getRequestDispatcher("addPage.jsp").forward(request, response);

		}
	
		if (action.equals("goUpdatePage")) {
			//we need the id here
			int userId = Integer.parseInt(request.getParameter("id"));
			oldUserId = userId;
			request.getRequestDispatcher("updatePage.jsp").forward(request, response);
			

		}
		if (action.equals("SearchByFilter")) {
			String searchText = request.getParameter("searchText");
			String searchFilter = request.getParameter("filter");
			users = UserDao.search(searchText, searchFilter);
			request.setAttribute("users", users);
			request.getRequestDispatcher("HomePage.jsp").forward(request, response);

		}
		else {
			request.setAttribute("users", users);
			request.getRequestDispatcher("HomePage.jsp").forward(request, response);
			
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.print("I'm in post method");
		
		// TODO Auto-generated method stub
		List<User> users = null;
		String userName = request.getParameter("userName");
		String password = request.getParameter("password");
		String email = request.getParameter("email");
		String phone = request.getParameter("phone");
		String city = request.getParameter("city");
		String cin = request.getParameter("cin");
		String searchText = request.getParameter("searchText");
		String searchFilter = request.getParameter("searchFilter");
		System.out.print("\n"+searchText);
		System.out.print("\n"+searchFilter);
		

		try {
			if(action==null) {
				users = UserDao.search(searchText, searchFilter);
				request.setAttribute("users", users);
				request.getRequestDispatcher("HomePage.jsp").forward(request, response);
				
			}
			else if (action.equals("goAddPage")) {
				System.out.print("at add method");
				UserDao.add(userName, password, email, phone, city, cin);
				users = UserDao.getUsers();
				request.setAttribute("users", users);
				action="listUsers";
				request.getRequestDispatcher("HomePage.jsp").forward(request, response);

			}
			else if (action.equals("goUpdatePage")) {
				
				System.out.print("at Update method");
				
				UserDao.update(oldUserId, userName, password, email, phone, city, cin);
				users = UserDao.getUsers();
				request.setAttribute("users", users);
				action="listUsers";
				request.getRequestDispatcher("HomePage.jsp").forward(request, response);
				

			}
		
			

		} catch (Exception e) {

			e.printStackTrace();
		}

	}

	protected void doSave(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		out.print(request.getProtocol());
	}

}
