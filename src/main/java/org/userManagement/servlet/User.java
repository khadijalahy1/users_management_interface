package org.userManagement.servlet;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

// it allows us the access to the database 
// il fait l'analogie entre database et Objet : Mapping  tt  Attribut(table) --->attribut(javabeans)


@Entity
@Table(name = "\"users\"", schema = "\"public\"")
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name="user_generator", sequenceName = "user_seq", allocationSize=50)
	@Column(name = "\"userid\"", updatable = false, nullable = false)
    private int id;
	
	@Column(name = "\"username\"")
    private String userName;
	
	@Column(name = "\"pssword\"")
    private String password;
	
	@Column(name = "\"email\"")
    private String email;
	
	@Column(name = "\"phone\"")
    private String phone;
	
	@Column(name = "\"city\"")
    private String city;
	
	@Column(name="\"cin\"")
	private String cin;
 
    public int getId() {
        return id;
    }
 
    public void setId(int id) {
        this.id = id;
    }
 
    public String getCin() {
		return cin;
	}

	public void setCin(String cin) {
		this.cin = cin;
	}

	public String getUserName() {
        return userName;
    }
 
    public void setUserName(String userName) {
        this.userName = userName;
    }
 
    public String getPassword() {
        return password;
    }
 
    public void setPassword(String password1) {
        this.password = password1;
    }
 
    public String getEmail() {
        return email;
    }
 
    public void setEmail(String email) {
        this.email = email;
    }
 
    public String getPhone() {
        return phone;
    }
 
    public void setPhone(String phone) {
        this.phone = phone;
    }
 
    public String getCity() {
        return city;
    }
 
    public void setCity(String city) {
        this.city = city;
    }

 
    
    
}

