<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" href="homePage.css">
</head>
<body>

	<div class="container">





		<!--  Introduction  -->

		<h1>Users informations</h1>
		<!--  Search Bar  -->




		<form action="UserControlerServlet" method="POST">
			<div class="form-control">
			
			<label for="search"><i class="icon-search"></i></label> <input
					class="table-filter" type="search" data-table="advanced-web-table"
					placeholder="Search..." name="searchText"> 
			
			
					<input
					class="btn" type="submit" value="Search" />
			
			</div>
			
				
				<!--  Filter  -->
				<div class="form-control">
				
				<button type="button" class="btn_choose_sent bg_btn_chose_1">
							<input type="radio" name="searchFilter" value="userName" checked />UserName
						</button>
						<button type="button" class="btn_choose_sent bg_btn_chose_2">
							<input type="radio" name="searchFilter" value="email" />Email
						</button>
						<button type="button" class="btn_choose_sent bg_btn_chose_3">
							<input type="radio" name="searchFilter" value="phone" />Phone
						</button>
						<button type="button" class="btn_choose_sent bg_btn_chose_3">
							<input type="radio" name="searchFilter" value="city" />City
						</button>
						<button type="button" class="btn_choose_sent bg_btn_chose_3">
							<input type="radio" name="searchFilter" value="cin" />Cin
						</button>
				

				
						

					</div>
		</form>


		<div class="form-control">
			<c:url value="UserControlerServlet" var="addPage">
				<c:param name="action" value="goAddPage" />
			</c:url>
			<a href="${addPage}"><button class="btn">add User</button></a>
		</div>










		<!--  Table  -->

		<p class="table-responsive">
		<table id="ordering-table" class="advanced-web-table">
			<thead>
				<tr>
					<th>ID</th>
					<th>UserName</th>
					<th>password</th>
					<th>email</th>
					<th>phone</th>
					<th>city</th>
					<th>cin</th>
					<th>update</th>
					<th>Delete</th>

				</tr>
			</thead>

			<tbody>
				<c:forEach items="${users}" var="user">

					<tr>
						<td>${user.getId()}</td>
						<td>${user.getUserName()}</td>
						<td>${user.getPassword()}</td>
						<td>${user.getEmail()}</td>
						<td>${user.getPhone()}</td>
						<td>${user.getCity()}</td>
						<td>${user.getCin()}</td>
						<td><c:url value="UserControlerServlet" var="updatePage">
								<c:param name="action" value="goUpdatePage" />
								<c:param name="id" value="${user.getId()}" />
							</c:url> <a href="${updatePage}"><button class="btn">update</button></a></td>
						<td><c:url value="UserControlerServlet" var="deleteUser">
								<c:param name="action" value="delete" />
								<c:param name="id" value="${user.getId()}" />
							</c:url> <a href="${deleteUser}"><button class="btn">delete</button></a></td>
					</tr>
				</c:forEach>






			</tbody>
		</table>
		</p>
	</div>

</body>
</html>